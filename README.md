# Guia de Configuração de Ambiente de Desenvolvimento para Linux Mint

## Java

[Como instalar multiplas versões do jdk](./ambiente/java/java.md)

## InteliJ

[Como Configurar o InteliJ](./ambiente/intelij/inteliJ.md)

## Dicas de Desenvolvimento

[Clique aqui](./ambiente/dicas-dev/dicas.md)


## Links Úteis

### Documentação

[das propriedades comuns da aplicação (application.properties)](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html) <br>
[Spring Data JPA](https://spring.io/projects/spring-data-jpa#overview) <br>
[Lombok](https://projectlombok.org/) <br>


### Livros
[Livro azul famoso de DDD](https://martinfowler.com/bliki/DDD_Aggregate.html) <br>

### Tutoriais
[Entendendo o equals e hashCode](https://blog.algaworks.com/entendendo-o-equals-e-hashcode/) <br>
[Tutorial definitivo: Tudo o que você precisa para começar bem com JPA](https://blog.algaworks.com/tutorial-jpa/) <br>
[Entenda a classe Optional do Java](https://blog.algaworks.com/chega-de-nullpointerexception/) <br>
[Documentação do Spring Data JPA: Keywords de query methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation) <br>
[Logback](http://logback.qos.ch/) <br>
[SLF4J](http://www.slf4j.org/) <br>
[Especificação do Problem Details for HTTP APIs (RFC 7807)](https://datatracker.ietf.org/doc/html/rfc7807) <br>
[Constraints do Bean Validation](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#section-builtin-constraints) <br>
[Modelmapper](http://modelmapper.org/) <br>
[UUID](https://www.uuidgenerator.net/) <br>

[GitHub Gist da estrutura do arquivo orm.xml](https://gist.github.com/thiagofa/35d5a651a39cb0b26f050dc3b1ce8f9b) <br>
[Gnutls Logo The GnuTLS Transport Layer Security Library](https://gnutls.org/) <br>
[Requisições simples](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#simple_requests) <br>
[Processamento de template de corpo de e-mails](https://freemarker.apache.org/) <br>
