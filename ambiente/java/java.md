
# Java

#### Links Úteis

[OpenJDK 12 da AdoptOpenJDK](https://adoptopenjdk.net/releases.html?variant=openjdk12&jvmVariant=hotspot) <br>
[OpenJDK 11 da Oracle (sem suporte e sem updates de segurança](https://jdk.java.net/archive/) <br>
[JDK 11 da Oracle (não pode ter uso comercial sem uma licença)](https://www.oracle.com/br/java/technologies/javase/jdk12-archive-downloads.html) <br>
[Convenção de Código](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf)


Execute o comando:
```shell 
$  wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo
```

Agora adicione o repositório do AdoptOpenJDK com o comando:

```shell
$ sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
```

Atualize os repositórios:

```shell
$ sudo apt update
```

Agora execute a instalação do OpenJDK:

```shell
$ sudo apt install adoptopenjdk-12-hotspot
```

Aguarde a instalação ser concluída.

Se você tiver mais de um JDK na sua máquina você poderá decidir qual deles utilizar seguindo os comandos abaixos:

```shell 
$ sudo update-alternatives --config java
```
Você Poderá escolher entre as opções abaixo:

![](../imagens/excolher-versao-java.png "Escolher Versão Java")

Escolha a opção abaixo:

```shell 
$  1            /usr/lib/jvm/adoptopenjdk-12-hotspot-amd64/bin/java   1121      modo manual
```

Confira se o JDK foi instalado com sucesso:

```shell
$ java -version
$ java --version
```
