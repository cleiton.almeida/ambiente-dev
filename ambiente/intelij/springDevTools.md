## InteliJ

### Configurar Spring Dev Tools (Opcional)

O Spring Boot Devtools é configurado pelo JHipster e irá "reiniciar" sua aplicaão quando as classes do seu projeto forem compiladas.
<br>

Por padrão, o InteliJ não compila arquivos automaticamente quando a aplicação está em execução.<br>
Para ativar o recurso "Compile on save" basta seguir os passos abaixo:

Vá até ``File -> Build, Execution, Deployment -> Compiler`` e habilite a opção ``Make project automatically``.

Abra a janela de ação com o seguinte comando:
* Linux: ``CTRL + SHIFT + A``
* Mac OSX: ``SHIFT + COMMAND + A``
* Windows: ``CTRL + ALT + SHIFT + /`` 

OBS: No meu caso, mesmo utilizando linux mint o comando que funcionou no meu InteliJ foi a opção para Windows.


