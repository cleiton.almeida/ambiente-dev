# Dicas de Desenvolvimento

### Boas práticas para trabalhar com data e hora em REST APIs

1) Usar ISO-8601
2) Aceite qualquer fuso horário
3) Armazene em UTC
4) Retorne em UTC
5) Não inclua o horário, senão for necessário.
